**Instagram Clone using Firebase**
This application was built as a project within [this course](https://www.letsbuildthatapp.com/course/Instagram-Firebase "LBTA's Course") by Brian Voong from Let's Build That App (LBTA). It replicates many of the most relevant features from the popular Instagram app, for social networking and photo sharing.

The main functionalities offered by this app are the following:
- User registration and login, using email
- Creating new posts, by taking new photos or importing from gallery, and adding a description
- Multi-user comments on existing posts
- User search, user profile pages and following users
- Grid and List view for user profile pages
- Push notifications upon getting a new follower

Several of Firebase's functionalities were used within this project:
- Firebase Auth, for user creation and authentication
- Firebase Database, for storing posts, comments and likes information
- Firebase Storage, for storing user profile and post images
- Firebase Messaging / Cloud Functions, for sending push notifications for new followers

Relevant iOS frameworks used:
- AVFoundation, for capturing new photos