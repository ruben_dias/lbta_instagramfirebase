//
//  User.swift
//  InstagramFirebase
//
//  Created by Ruben Dias on 22/04/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation

struct User {
    
    let uid: String
    let username: String
    let profileImageURL: String
    
    init(uid: String, dictionary: [String: Any]) {
        self.uid = uid
        username = dictionary["username"] as? String ?? ""
        profileImageURL = dictionary["profileImageURL"] as? String ?? ""
    }
}
