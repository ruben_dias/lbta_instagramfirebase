//
//  Post.swift
//  InstagramFirebase
//
//  Created by Ruben Dias on 01/04/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation

struct Post {
    
    var id: String?
    let user: User
    let imageURL: String
    let caption: String
    let creationDate: Date
    var hasLiked = false
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.imageURL = dictionary["imageURL"] as? String ?? ""
        self.caption = dictionary["caption"] as? String ?? ""
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}
